$(window).scroll(function() {
  $("nav").toggleClass("scrolled-nav", $(this).scrollTop() > 100);
});

$(".carousel").carousel({
  interval: 7000
});
